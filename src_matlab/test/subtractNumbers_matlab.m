function result = subtractNumbers_matlab(a, b)
    % Subtract two numbers with MATLAB
    %
    % Parameters
    % ----------
    %   a: double
    %       Input value 1
    %   b: double
    %       Input value 2
    %
    % Returns
    % -------
    %   The input values subtracted
    %
    % Example
    % -------
    % sumNumbers_matlab(2,2)
    result = a - b;
    end
    
    